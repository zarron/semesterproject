# Todo

## Design
- [x] Key Management
- [ ] Key Server Authentication
- [ ] Message Properties
- [ ] Add Specification for API Class

## UI

## Encryption
- [ ] Encrypt Function
- [ ] Decrypt Function

## API
- [x] Create Telegram Client Account
- [x] Check Facebook API

## 