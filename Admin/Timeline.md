# Timeline
```mermaid
gantt
    dateFormat  DD-MM-YY
    title       Encrypted Messaging
    excludes    weekends

    section Administrative


    section Work
    Setup Plan                          :des1, 12-03-21, 5d
    Choose Framework/Libraries          :des2, after des1, 5d
    Implementation                      :des3, after des2, 10w
```