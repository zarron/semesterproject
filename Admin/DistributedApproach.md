# Distributed Approach

## Problems with previous approach

### No Server Authentication
Because only encrypted data was stored on the server, I proposed to skip authentication to lower the trust needed in the server. But since in would improve security, authentication could just be added again.

### Forward Secrecy

#### Old Message Transmission
One approach would be to just use the signal protocol and transmit old messages from an old device to a new one on setup of the new device. But this would be pretty close to signal, since it would actually just add a transmission of the database and nothing else.

#### Free Server Choice
A bit more interesting approach would be to go for a distributed system, which would also go well with the name of the group :wink:. Here the goal would be to make the server code open source as well, and let the clients choose which servers they want to use (with the possibility of self hosting). 

Here one could either apply the signal protocol, or still discuss if under this circumstances a (not forward secure) cloud solution would be feasible.

Like this, the clients would be free to use this messenger only in a VPN, use IP whitelists for the server, etc.

## Brainstorming

### Market Niche
![Convenience vs Security](images/security_convenience.png)

### 2-Layer Security
![NFC](images/nfc.png)