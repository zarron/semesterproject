# Threema

## Normal Messages
Encrypted with a key that is derived by hashing a asymmetric key. They are encrypted together with a nonce. The whole message sent is a MAC, the Ciphertext and the Nonce. 
 
## Groups
Each message is sent to every group member, media files are symmetrically encrypted and stored on an server. Their symmetry key is sent do the group members.
This means that groups are not known to the servers.

## Forward Secrecy
Not forward secret, public/private key pair is linked to account forever. But Client/Server encryption is forward secret

## Security
- Not Forward Secure
- End-to-End encrypted
- Replay protected (Nonce)


## Summary


# Whatsapp

## Normal Messages
On installation the app generates 3 types of keys. A long term key, a medium term key and a few single use keys. They then get stored on the whatsapp server.

If a new chat is created, the remote public keys are fetched, and with the own private keys 4 new keys are generated, which are combined to a master secret

Then a root key and a chain key is created from the master secret.

To send a message, the chain key is used to create a symmetric key to encrypt a message. They chain key is then updated with a hash of itself (ratched).

Every time a message is roundtripped, a new chain key is created.

Media messages is solved as in Threma.

## Groups
The first time a group member sends a message, it sends its public signing key and a random symmetric chaining key individually to each group member. Then this symmetric key is used to send signed messages to the group (distributed and multiplied by the server). If a member leaves, the chaining key is resent.

# Zoom

## Initialization
On meeting start, the participants sign in at the server, using their long term key pair. 

The leader generates a symmetric key, which is distributed to all participants. Therefore packets can be multiplexed


## MitM
To prevent MitM attacks, every participant calculates a hash of the leaders signing values. They can be read out aloud by the leader.

