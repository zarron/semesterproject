# Implementation

## Requirements
The application should meet the following requirements:

### Secrecy 
The messages should be kept secret from everybody except the sender and receiver. The server also should not be able to read the messages.

### Integrity 
An attacker should not be able to alter the message without it being detected.

### Authenticity 
The receiver should be able to verify if the message really came from the claimed sender.

### Cloud
Messages should be saved in the cloud, such that they can be accessed from anywhere.

### Forward Secrecy
Since the user should be able to log in from a new device, without contacting an old one, but seeing its old messages anyway, forward secrecy is not achievable (I am not 100% sure about this, but I strongly believe :interrobang:).

### Complexity
Since we are dealing with small group chats and small message sizes, the complexity to send a message in a group chat can be $`\mathcal{O}(n²)`$ where $`n`$ is the number of group members. For files like photos/videos however, this is not suitable, here a $`\mathcal{O}(n)`$ complexity is desired.

### Server Trust
One should have to trust the server as little as possible.

## Keys
### Clients
The clients need an unique signing keypair (e.g. $`IK_A`$) to sign their messages.
Furthermore they need a symmetric key for each channel they establish with another client (private chat or group chats)

### Server
The server could have an unique keypair to sign the signing keys of the clients, this would be necessary, if the clients would have to register with their phone number or the link.

Furthermore the server has to be a cache for public pre keys that the clients created (e.g. $`PB_{B1}`$). They can then be fetched by other clients to initiate a conversation, even if the client is offline. 

This setup is similar to the signal protocol, but for now without the backup option [1].

![keys](images/keys.png "Keys")

## Key Exchange

If $`A`$ wants to establish connection with $`B`$, she has to establish a new symmetric key, in a similar procedure to the signal protocol [1] is applied:

1. $`A`$ generates an ephemeral key pair $`E_A`$ and fetches local identity key ($`IK_{A}`$)
2. $`A`$ fetches a pre key of $`B`$ ($`PK_{B1}`$), and the identity key of $`B`$ ($`IK_{B}`$) from the server
3. $`A`$ now generates $`DH_1 = DH(E_A, IK_{B})`$ and $`DH_2 = DH(PK_{B1}, IK_{A})`$
4. $`DH_1`$ and $`DH_2`$ are now combined in a key derivation function [HKDF], and the symmetric key ($`S_{AB}`$) is obtained by $`A`$
5. $`A`$ now sends her first message with payload, encrypted with the symmetric key, but also the not encrypted but MACed $`E_A`$ public key
6. $`B`$ can now calculate the same symmetric key $`S_{AB}`$

## Message Construction

### Secrecy
To ensure Secrecy, the message must be encrypted. Furthermore to prevent replay attacks, the message should contain also a nonce, which is also encrypted. For encryption the AES-128-GCM algorithm could be used (Here I think I do not know enough to make reasonable assumptions, but as far as I know from my NetSec lecture AES is considered state of the art for symmetric encryption, and it includes also integrity :interrobang:) [2]

### Integrity
Since AES-128-GCM is used, there is no need for an additional integrity check (As far as I understand, correct me if I'm wrong :interrobang:).

### Authenticity 
Since to derive the symmetric key one needs the private key to its own identity key, it cannot be forged by anyone else than $`A`$ or $`B`$. Therefore no further authentication method is needed. Given that the public identity keys really belong to the person they claim (see thoughts).

![message](images/message.png)

## Group Encryption
Since groups are small, and messages as well, one could just send a group message to every group member personally, encrypted with the algorithm above. But this would not only lead to very many messages to send, but also every message must be stored $`n`$ times no the server, where $`n`$ is the number of group participants. It is what Threema does, but it can be done better [3].

An easy approach would be to just use a symmetric key that is known by all group members. This would be very efficient, but it introduces a problem when a group member leaves the group, or a new one joins the group. Both could see all messages sent in the past and in the future, even if they were not part of the group at this time.

Therefore the symmetric key ($`S_{GID, SID}`$) must be changed every time the group composition changes. In the following a time span during which the group composition does not change is called a _session_. The creation of this key is described in the section _Join/Leave Group_

This introduces a new problem. The messages are now encrypted with different keys. Therefore the message must be labeled with the group id ($`GID`$) that uniquely identifies the group, and with a session id ($`SID`$) that determines the session during which the message was sent, i.e. which symmetric key was used.

Note that this does not increase forward secrecy, since the session keys are not discarded after the group composition changes and a new key is generated. They are saved to decrypt older messages.

![group message](images/group_message.png)

One problem still remains. In the current setup, authenticity is no longer given. Since everybody in the group knows the symmetric session key, everybody can just send a message, and spoof the sender identity. To solve this I have two proposals, where I think the first is better, but I am not 100% sure :interrobang:.

**Asymmetric Signing**
A simple way would be that sender $`A`$ just signs every message she sends, but as far as i know signing is a quite costly cryptographic operation, and it may not be feasible to do it for every message, but I am not sure. :interrobang:

**Symmetric Signing**
To avoid using asymmetric cryptography with every message, $`A`$ could generate a symmetric key with every group peer ($`S_{AB}`$). These could be long lived. Importantly, they are only known by $`A`$ and the one corresponding peer (e.g. $`B`$). $`A`$ could then calculate a keyed hash of the message and every symmetric key. These calculated hashes could then be attached to the message, to authenticate $`A`$.
It would save calculation time (for small numbers of group members), but the messages would now have a size of $`\mathcal{O}(n)`$ and no longer $`\mathcal{O}(1)`$ and it is more complicated to implement.

![Symmetric Signing](images/group_auth.png)

## Group Media
Since Threema and WhatsApp/Signal send the group messages to every member individually, sending an image like this would be quite a waste of resources. Therefore, they have a own protocol for sending media/large messages. For the protocol proposed here, it is not necessary to have a separate treatment for large messages, since they are sent only once anyway.

## Join/Leave Group
If a new member joins the group, the session key generation is easy. The new member just generates a random key and sends it in private messages to all other members. This message contains also the new session ID (it is continues).

Leaving is a bit harder. Here the leaving member cannot simply generate a new session key before he leaves, because like this he could still read all messages sent after he left.

Therefore the first member to send a message after the leaving has to announce a new symmetric key. Here we have the problem of race conditions (see example image below, where $`A`$ and $`C`$ announce a new key at the same time). To solve it, the first time a group member receives an announcement he forwards it to every other member of the group. Now each client should accept the key that was announced the most times to it. In case of a tie, the announcement of the sender with the lowest ID is chosen (in this case $`A`$) This has complexity of $`\mathcal{O}(n^2)`$ which is acceptable.

Like this, another problem is solved as well. If the announcer of a new session key would not send the key to a specific group member (on purpose), this member would not be able to communicate anymore. But because the announcements are flooded, this attack won't work. (Except if more than half of the members participate, and send wrong advertisements)

Messages already sent with a key that "lost" the vote must be resent.

![race condition](images/race_condition.png)
Race Condition

![advertisement flooding](images/flooding.png)
$`B`$ and $`C`$ received $`C`$s advertisement first, and therefore propagated it. This means that $`C`$s advertisement won the "vote" 3 to 1 and will now be used.

## Cloud
### Multi Device
Since the clients should be able to log in from any device, all the private and symmetric keys a client needs should be saved on the server. Those should be encrypted with a password only the client knows. Like this, nobody except the client can decrypt them and thy could even be queried from the server without client authentication.

![cloud](images/cloud.png)

### Other Data

The same way, messages could also be saved/fetched on the server without any authentication, since only the clients can make sense of them.

To upload pre keys (e.g. $`PK_{B1}`$) the client should authenticate itself, this can be done via the identity key.

## Identity Key Verification
To verify the identity key of a peer, a system to scan a QR code of the identity key of the peer could be implemented, link in Threema.

## Problems to solve
A few problems remain, that are not too important yet, and can be solved later:

- store metadata of client on the server (in which groups am I, profile picture, etc.) with zero trust in the server
- store group metadata on server (group profile picture, participants list, etc.) with zero trust in the server

## Thoughts

### Forward Secrecy

The Ratching Algorithm used by WhatsApp (and therefore Signal) would be really nice to provide forward secrecy, but I think it is not possible to use it if one still wants to be able to restore the full chat history from the cloud on a new device. Otherwise the initial symmetric key must be stored as well on the cloud server, and then forward secrecy is no longer guaranteed.

### Nonce

I think since the nonce is encrypted, and its only purpose is to prevent replay attacks, it could actually be publicly known, i.e. it would start at 0 for every new chat. The sender would not accept a duplicated message, and an attacker could not forge a message with a specific nonce, due to encryption.
In any way, the nonce must be continuous, such that a missing message can be detected (e.g. deleted by a malicious server) 


[1]: https://signal.org/docs/specifications/x3dh/
[2]: papers/whatsapp_whitepaper.pdf
[3]: papers/threema_whitepaper.pdf