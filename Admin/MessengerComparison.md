# Messenger Comparison

|                      | ![Whatsapp](images/whatsapp.png)           | ![Telegram](images/telegram.png)       | ![Signal](images/signal.png)  | ![Threema](images/threema.png)             |
| -------------------- | ------------------------------------------ | -------------------------------------- | ----------------------------- | ------------------------------------------ |
| E2E Encryption       | All chats                                  | secret personal only                   | All chats                     | All chats                                  |
| Multi Device         | App is base, other clients only mirror app | full multi device support              | Sesame Algorithm [1]          | App is base, other clients only mirror app |
| Cloud                | Unencrypted backup option on Google Drive  | Full cloud support, except secret chat | Caching only                  | Caching only                               |
| Open Source (Client) | :x:                                        | :heavy_check_mark:                     | :heavy_check_mark:            | :heavy_check_mark:                         |
| Account              | Phone number, no password                  | Phone number, no password              | Phone number, no password     | Phone number, no password                  |
| Identity Check       | Code scan                                  | Manual code check :man_shrugging:      | code scan                     | code scan                                  |
| Groups               | Signal Groups [3]                          | not E2E encrypted                      | Signal Groups [3]                | Messages sent to every member individually |
| Forward Secrecy      | new key for every message, if no backups   | rekeying every 100 messages [2]        | new key for every message [4] | long term keys                             |


## Sesame Algorithm
[1]: https://signal.org/docs/specifications/sesame/#introduction
In short: Every device of an user has its own key pair, and every message is sent individually to each device.


## Signal Groups
[3]: papers/whatsapp_whitepaper.pdf
In short: On creation, each sender sends a randomly generated personal sender key to all other members individually. This key is used to send messages that are fanned out by the server. It is also ratched.


## Forward Secrecy
![Forward Secrecy](images/forward_secrecy.png)


[2]: https://core.telegram.org/api/end-to-end/pfs
[4]: https://signal.org/docs/specifications/doubleratchet/