# Security
- E2E encrypted
- forward secure? Probably not possible when cloud based 
- Messages should be singed and MACed
- Replay Protection with Nonce
- A Threema like Public Key Identity check would be nice

# Infrastructure
- Messages should be stored on a central server.
- Probably this server would also store the (encrypted) private keys of the clients

# Group Chats