import express from "express";
import crypto from "crypto"
import { SignJWT } from 'jose/jwt/sign'
import { jwtVerify } from 'jose/jwt/verify'
import fs from "fs"

const app = express();
const port = 8080; // default port to listen

const { privateKey, publicKey } = crypto.generateKeyPairSync('ed25519', {
    //namedCurve: 'sect239k1'
  });
  

/*
const { publicKey, privateKey } = crypto.generateKeyPairSync('ed25519', {
    //modulusLength: 4096,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem'
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem'
    }
  });
*/

fs.writeFileSync('public.pem', publicKey.export({type: "spki", format: "pem"}));
fs.writeFileSync('private.pem', privateKey.export({type: "pkcs8", format: "pem"}));

/*
, {
    publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
    },
    privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
    }
}
*/
// define a route handler for the default home page
app.get("/", async (req, res) => {

    var privateKey = fs.readFileSync('private.pem');

    console.log(crypto.createPrivateKey(privateKey).asymmetricKeyType)

    let signedToken = await new SignJWT({'id':''})
    .setProtectedHeader({ alg: 'EdDSA' })
    .setIssuedAt()
    .setExpirationTime('2h')
    .sign(crypto.createPrivateKey(privateKey))


    console.log(signedToken)

    var publicKey = fs.readFileSync('public.pem'); 

    const { payload, protectedHeader } = await jwtVerify(signedToken, crypto.createPublicKey(publicKey))

    console.log(payload)
    res.send(payload)

});

// start the express server
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});

